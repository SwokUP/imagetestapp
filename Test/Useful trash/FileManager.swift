////
////  FileManager.swift
////  Test
////
////  Created by DeveloperMBP on 6/1/19.
////  Copyright © 2019 DeveloperMBP. All rights reserved.
////
//
//import Foundation
//import UIKit
//import var CommonCrypto.CC_MD5_DIGEST_LENGTH
//import func CommonCrypto.CC_MD5
//import typealias CommonCrypto.CC_LONG
//
//// MARK: - magic variable?
//public let kPlaceholderIMG = UIImage(named: "placeholder") ?? UIImage()
//public let kNoImage = "NONAME"
//
//
//// TODO Crerate class for managing images (cache and store)?
//// TODO Store to memorry not using dictionary but other cache supporting class NSChace
///// TODO Transfer from file system to memory 
///// TODO Create method to chack if file exists and if not -> download with closure 
//
//class MediaFile {
//    
//    // MARK: - init
//    var imageName: String
//    var lat: Double
//    var long: Double
//    var url: String
//    var storedName: String
//    
//    /// TODO Transfer to closure 
//    
//    init(imageName: String, lat: Double, long: Double, url: String) {
//        self.imageName = imageName
//        self.lat = lat
//        self.long = long
//        self.url = url
//        self.storedName = kNoImage
//        
//    }
//    
//    // MARK: - Public
//    public func getImage(completion: @escaping (_ image: UIImage?) -> Void){
//        let fileManager = FileManager.default
//        if fileManager.fileExists(atPath:getMediaFilePath()) {
//            let fileUrl = URL(fileURLWithPath: getMediaFilePath(), isDirectory: false)
//            let img = UIImage(contentsOfFile: fileUrl.path)!
//            completion(img)
//        } else {
//            completion(kPlaceholderIMG)
//            RequestManager.shared.loadImageForMediaFile(mediaFile: self) { (image) in
//                self.saveImage(image: image)
//                completion(image)
//            }
//        }
//    }
//    
//    // MARK: - Private
//    private func saveImage(image: UIImage) {
//        
//        guard let documentsDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else { return }
//        
//        let fileURL = documentsDirectory.appendingPathComponent(storedName)
//        guard let data = image.jpegData(compressionQuality: 1) else { return }
//        
/////        Checks if file exists, removes it if so.
/////         why?
/////         if FileManager.default.fileExists(atPath: fileURL.path) {
/////           do {
/////               try FileManager.default.removeItem(atPath: fileURL.path)
/////               debugPrint("Removed old image")
/////           } catch let removeError {
/////               debugPrint("couldn't remove file at path", removeError)
/////           }
/////               }
//        do {
//            try data.write(to: fileURL)
//        } catch let error {
//            debugPrint("error saving file with error", error)
//        }
//    }
//    
//    // MARK: - Helpers
//    public func getMediaFilePath() -> (String) {
//        
//        // wow
//        // such many calls of cache path
//        // no defines for it
//        // code duplication
//        
//        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
//        let cachesDirectory: AnyObject = paths[0] as AnyObject
//        let dataPath = (cachesDirectory as! String) + "/" + storedName
//        
//        return dataPath
//    }
//    
//    public func mediaFileName() -> String {
//        /// why MD5?
//        let md5Data = MD5(string: url)
//        let fileName = md5Data.map { String(format: "%02hhx", $0) }.joined()
//        
//        return fileName + ".jpg"
//    }
//    
//    private func MD5(string: String) -> Data {
//        let length = Int(CC_MD5_DIGEST_LENGTH)
//        let messageData = string.data(using:.utf8)!
//        var digestData = Data(count: length)
//        
//        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
//            messageData.withUnsafeBytes { messageBytes -> UInt8 in
//                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
//                    let messageLength = CC_LONG(messageData.count)
//                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
//                }
//                return 0
//            }
//        }
//        return digestData
//    }
//}
//
