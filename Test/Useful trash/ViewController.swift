////
////  ViewController.swift
////  Test
////
////  Created by DeveloperMBP on 5/31/19.
////  Copyright © 2019 DeveloperMBP. All rights reserved.
////
//
//import UIKit
//
//class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    
//    /// You need it from elsewhere?
//    // MARK: - Variables
//    @IBOutlet private weak var selectedImageView: UIImageView!
//    @IBOutlet private weak var imagesCollectionView: UICollectionView!
//    @IBOutlet private weak var imageTitleLabel: UILabel!
//    @IBOutlet private weak var descriptionTitleLabel: UILabel!
//    @IBOutlet private var imageTapGesture: UITapGestureRecognizer!
//    
//    
//    private var imgArray = [MediaFile]()
//    /// put on storyboarad?
//    
//    private var selectedMediaFile: MediaFile?
//    
//    // MARK: - life cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        setup()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//    private func setup() {
//        /// the optionals in Swift are not just beacuse, add a check if it is nil
////        if let array = FileStorageManager.shared.requestMediaFilesArray() {
////           imgArray = array
////        }
//        
//        imageTapGesture.addTarget(self, action: #selector(tap))
//    }
//    
//    // MARK: - Actions
//    @objc private func tap() {
//        guard (selectedMediaFile != nil) else {
//            return
//        }
//        performSegue(withIdentifier: "toWeb", sender: self)
//    }
//    
//    
//    // MARK: - UICollectionViewDelegate
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return imgArray.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
//        
//        let mediaFile = imgArray[indexPath.row]
//        
//        cell.setupCell(mediaFile: mediaFile)
//        
//        return cell
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        
//        selectedMediaFile = imgArray[indexPath.row]
//
//        if let selected = selectedMediaFile {
//        selected.getImage(completion: { (image) in
//            DispatchQueue.main.async {
//                self.selectedImageView.image = image
//            }
//        })
//            
//        imageTitleLabel.text = selected.imageName
//        let description = "lat: " + selected.lat.description + " long: " + selected.long.description
//        descriptionTitleLabel.text = description
//        }
//    }
//    
//    // make using UICollectionViewLayout
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let viewWidth = (view.bounds.size.width / 3) - 12
//        
//           return CGSize(width: viewWidth, height: viewWidth)
//    }
//    
//    // MARK: - StoryboardSegue
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard segue.identifier == "toWeb" else {
//            return
//        }
//     
//        if let viewController: WebViewController = segue.destination as? WebViewController {
//            viewController.mediaFile = selectedMediaFile
//            
//        }
//    }
//}
