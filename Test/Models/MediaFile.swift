//
//  MediaFile.swift
//  Test
//
//  Created by DeveloperMBP on 6/15/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation


class MediaFile {

    // MARK: - init
    var imageName: String
    var lat: Double
    var long: Double
    var url: String
    var storedName: String

    /// TODO Transfer to closure 
    init(imageName: String, lat: Double, long: Double, url: String, storedName:String) {
        self.imageName = imageName
        self.lat = lat
        self.long = long
        self.url = url
        self.storedName = storedName

    }
}
