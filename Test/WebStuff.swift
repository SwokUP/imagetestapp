//
//  VebStaf.swift
//  Test
//
//  Created by DeveloperMBP on 6/5/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

/// may be WebStuff?

import Foundation
import WebKit

/// why inherit from ViewController
class WebViewController: UIViewController {
    
    // MARK: - Variables
    var mediaFile: MediaFile! // mediaFile?
    @IBOutlet private weak var webView: WKWebView!
    
    // MARK: - life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let htmlString = getHtmlString() else {
            return
        }
        webView.loadHTMLString(htmlString, baseURL: URL(fileURLWithPath: AppConstant.cachesDirectory.appendingPathComponent(mediaFile.storedName).absoluteString))
        /// LET ME BACK FROM HEREEEEEEE, PLEEEEEEEEEEEEASE
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Private
    private func getHtmlString() -> String? {

        guard let fileURL = Bundle.main.url(forResource: "index", withExtension: "html"), var text = try? String(contentsOf: fileURL) else {
            debugPrint("fileURL is nil")
            return nil
        }

        text = text.replacingOccurrences(of: "__URL__", with: mediaFile.url)
        text = text.replacingOccurrences(of: "__TEXT__", with: mediaFile.imageName)
        text = text.replacingOccurrences(of: "__LOCURL__", with: AppConstant.cachesDirectory.path + "/" + mediaFile.storedName)

        return text
    }
}
