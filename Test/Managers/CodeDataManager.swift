//
//  CodeDataManager.swift
//  Test
//
//  Created by DeveloperMBP on 6/15/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager  {
    
    // MARK: - Variables
    static let shared = CoreDataManager()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "StoredMediaFileModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Public
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    public func saveMediaFileInDB(mediaFile: MediaFile, isSuccess: @escaping (_ success:AppConstant.DataStoreStatus) -> ()) {
                
        isMediaFileAlreadyExists(mediaFile: mediaFile) { [weak self] (status) in
            guard status == AppConstant.DataStoreStatus.Failure else {
                isSuccess(AppConstant.DataStoreStatus.AlreadyExists)
                return
            }
            
            self?.persistentContainer.performBackgroundTask { (context) in
                let item = NSEntityDescription.insertNewObject(forEntityName: MediaFileModel.entity().name!, into: context) as? MediaFileModel
                
                item?.imageName = mediaFile.imageName
                item?.url = mediaFile.url
                item?.lat = mediaFile.lat
                item?.long = mediaFile.long
                item?.storedName = String().MD5String(string: mediaFile.url)
                
                do {
                    try context.save()
                    debugPrint("Context saved")
                    isSuccess(AppConstant.DataStoreStatus.Success)
                } catch {
                    debugPrint("Failed saving")
                    isSuccess(AppConstant.DataStoreStatus.Failure)
                }
            }
        }
    }
  
    public func getArrayOfMediaFiles(isSuccess: @escaping (_ array: [MediaFile]) -> ()) {
        
        persistentContainer.performBackgroundTask { (context) in
            let request = NSFetchRequest<MediaFileModel>(entityName: MediaFileModel.entity().name!)
            do{
                
                var mediaFileArray:[MediaFile] = []
                
                let array = try context.fetch(request)
                for object in array {
                    guard let imageName = object.imageName,
                        let url = object.url,
                        let storedName = object.storedName else {
                        continue
                    }
                    
                    let sharedObject = MediaFile(imageName: imageName, lat:  object.lat, long: object.long, url: url, storedName: storedName)
                    mediaFileArray.append(sharedObject)
                }
                isSuccess(mediaFileArray)
            }catch{
                debugPrint("Failed")
            }
        }
    }
    
    public func isMediaFileAlreadyExists(mediaFile: MediaFile, completion: @escaping (_ exist: AppConstant.DataStoreStatus?) -> Void) {
        
        persistentContainer.performBackgroundTask { (context) in
            let request = NSFetchRequest<MediaFileModel>(entityName: MediaFileModel.entity().name!)
            request.predicate = NSPredicate(format: "storedName == %@", mediaFile.storedName)
            
            let result = try? context.fetch(request)
            
            if let result = result, result.isEmpty {
                completion(AppConstant.DataStoreStatus.Failure)
            } else {
                completion(AppConstant.DataStoreStatus.Success)
            }
        }
    }
}



