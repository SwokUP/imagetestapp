//
//  ImageCacheManager.swift
//  Test
//
//  Created by DeveloperMBP on 6/17/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit

class ImageCacheManager {
    
    // MARK: - Variables
    private var imageCache = NSCache<NSString, UIImage>()
    static let shared = ImageCacheManager()
    
    
    // MARK: - Public
    public func getImage(mediaFile: MediaFile, completion: @escaping (_ image: UIImage?) -> Void){
   
        let dataPath = AppConstant.cachesDirectory.appendingPathComponent(mediaFile.storedName)
        
        if let image = imageCache.object(forKey: mediaFile.storedName as NSString) {
            //From Cache
            debugPrint("image for " + mediaFile.imageName + " get from Cache")
            completion(image)
            return
        } else if FileManager.default.fileExists(atPath:dataPath.path) {
            //From Phone storage
            guard let image = UIImage(contentsOfFile: dataPath.path) else {
                completion(nil)
                return
            }
            self.imageCache.setObject(image, forKey: mediaFile.storedName as NSString)
            debugPrint("image for " + mediaFile.imageName + " was get from Phone storage")
            completion(image)
            return
        } else {
            //From RequestManager
            completion(UIImage(named: "placeholder"))
            RequestManager.shared.loadImageForMediaFile(mediaFile: mediaFile) { (image) in
                self.saveImageForMediaFille(image: image, mediaFile: mediaFile)
                debugPrint("image for " + mediaFile.imageName + " was get from RequestManager")
                completion(image)
                return
            }
        }
    }
    
    // MARK: - private
    private func saveImageForMediaFille(image: UIImage, mediaFile:MediaFile) {

        let fileURL = AppConstant.cachesDirectory.appendingPathComponent(mediaFile.storedName).absoluteString
       
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        do {
            try data.write(to: URL(fileURLWithPath: fileURL))
        } catch let error {
            debugPrint("error saving file with error", error)
        }
    }

}
