//
//  MediaFileManager.swift
//  Test
//
//  Created by DeveloperMBP on 6/17/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation

class MediaFileManager{
    
    // MARK: - Variables
    static let shared = MediaFileManager()

    
    // MARK: - Public
    public func getArrayOfMediaFiles(completion: @escaping (_ array: [MediaFile]?) -> ()) {
        
        var mediaFileArray = [MediaFile]()
        
        if let reachabilityManager = AppConstant.reachabilityManager, reachabilityManager.isReachable {
            if let jsonArray = RequestManager.shared.getJsonData()  {
                for object in jsonArray {
                    if let imageName = object["imageName"] as? String, let lat = object["lat"] as? Double, let long = object["long"] as? Double, let url = object["url"] as? String {
                        let imageModel = MediaFile(imageName: imageName, lat: lat, long: long, url: url, storedName: String().MD5String(string: url))
                        
                        CoreDataManager.shared.saveMediaFileInDB(mediaFile: imageModel) { (saved) in
                            debugPrint("Entity for " + imageModel.imageName + "save status", saved)
                        }
                        mediaFileArray.append(imageModel)
                    }
                }
            }
            completion(mediaFileArray)
        } else {
            CoreDataManager.shared.getArrayOfMediaFiles { (array) in
                completion(array)
            }
        }
    }
}
