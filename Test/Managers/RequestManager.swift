//
//  RequestManager.swift
//  Test
//
//  Created by DeveloperMBP on 6/1/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit

class RequestManager: NSObject {
    
    // MARK: - Variables
    //private override init() { }
    static let shared = RequestManager()
    private let session = URLSession(configuration: .default)
    
    // MARK: - Public
    public func getJsonData() -> [[String:Any]]? {
        /// TODO Transfer to guard 
        guard let fileURL = Bundle.main.url(forResource: "imageData", withExtension: "json") else {
            debugPrint("fileURL is nil")
            return nil
        }
        guard let data = try? Data(contentsOf: fileURL) else {
            debugPrint("data is nil")
            return nil
        }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
            debugPrint("json is nil")
            return nil
        }
        return json
    }
    
    public func loadImageForMediaFile(mediaFile: MediaFile, onSuccess: @escaping (_ image:UIImage) -> ())   {
 
        let task = session.dataTask(with: URL(string: mediaFile.url)!) { (data, response, error) in
            
            guard let imageData = data, let responseImage = UIImage(data: imageData) else {
                return
            }
            onSuccess(responseImage)
        }
        task.resume()
    }
}
