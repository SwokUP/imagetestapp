//
//  UIAlertControllerExtension.swift
//  Test
//
//  Created by DeveloperMBP on 6/15/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit

/// Create extension
extension UIAlertController {
    
    func showAlertWithMessage(message: String) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        
        AppConstant.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: {
            AppConstant.appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        })
    }
}
