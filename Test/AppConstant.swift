//
//  AppConstant.swift
//  Test
//
//  Created by DeveloperMBP on 6/15/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class AppConstant {
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
//    static var cachesDirectory: String {
//        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
//        let cachesDirectory: AnyObject = paths[0] as AnyObject
//        return (cachesDirectory as! String)
//    }
    
    static var cachesDirectory: URL {
        guard let path = URL(string: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]) else { return URL(string: "")!}
        return path
    }
    
    enum DataStoreStatus:String {
        case Success = "Saving was successful"
        case Failure = "Saving was a failure"
        case AlreadyExists = "File already exists"
    }
}

