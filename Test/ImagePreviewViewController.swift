//
//  ImagePreviewViewController.swift
//  Test
//
//  Created by DeveloperMBP on 6/15/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit

class ImagePreviewViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private enum Constants {
        static let CountColumn: CGFloat = 3
        static let CellIdentifier = "imageCell"
    }

    // MARK: - Variables
    @IBOutlet private weak var selectedImageView: UIImageView!
    @IBOutlet private weak var imagesCollectionView: UICollectionView!
    @IBOutlet private weak var imageTitleLabel: UILabel!
    @IBOutlet private weak var descriptionTitleLabel: UILabel!
    @IBOutlet private weak var imageTapGesture: UITapGestureRecognizer!
    
    private var imgArray = [MediaFile]()
    private var selectedMediaFile: MediaFile?

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    private func setup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        MediaFileManager.shared.getArrayOfMediaFiles { [weak self] (array) in
            
            guard let sArray = array, let self = self else {
                return
            }
            self.imgArray = sArray
        }
        
        imageTapGesture.addTarget(self, action: #selector(tap))
        
    }
    
    // MARK: - Actions
    @objc private func tap() {
        guard (selectedMediaFile != nil) else {
            return
        }
        performSegue(withIdentifier: "toWeb", sender: self)
    }
    
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifier, for: indexPath) as? ImageCollectionViewCell else { return UICollectionViewCell()}
        let mediaFile = imgArray[indexPath.row]
        
        cell.setupCell(mediaFile: mediaFile)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedMediaFile = imgArray[indexPath.row]

        if let selected = selectedMediaFile {
            ImageCacheManager.shared.getImage(mediaFile: selected) { (image) in
                DispatchQueue.main.async {
                    self.selectedImageView.image = image
                }
            }

            imageTitleLabel.text = selected.imageName
            let description = "lat: " + selected.lat.description + " long: " + selected.long.description
            descriptionTitleLabel.text = description
        }
    }
    
    
    // MARK: - StoryboardSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "toWeb" else {
            return
        }

        if let viewController = segue.destination as? WebViewController {
            viewController.mediaFile = selectedMediaFile
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    // make using UICollectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = (view.bounds.size.width / Constants.CountColumn) - 12

           return CGSize(width: viewWidth, height: viewWidth)
    }
    
}
