//
//  ImageCell.swift
//  Test
//
//  Created by DeveloperMBP on 6/3/19.
//  Copyright © 2019 DeveloperMBP. All rights reserved.
//

import Foundation
import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Variables
    @IBOutlet private weak var imagePreview: UIImageView!
    
    
    // MARK: - setupCell
    public func setupCell(mediaFile: MediaFile) {
        ImageCacheManager.shared.getImage(mediaFile: mediaFile) { [weak self] (image) in
            DispatchQueue.main.async {
                self?.imagePreview.image = image
            }
        }
    }
}
